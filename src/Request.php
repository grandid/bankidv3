<?php

namespace GrandID\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Exception;
use GuzzleHttp\Exception\RequestException;

class Request {

    var $client, $apiKey, $serviceKey;
    public $statusCode, $response;

    private $method = 'POST';

    public function __construct($type = 'production')
    {
        $url = ($type == 'test') ? "client-test.grandid.com" : "client.grandid.com";
        $this->client = new Client([
            'base_uri' => sprintf("https://%s", $url),
            'verify' => false
        ]);
    }

    public function __call($method, $args)
    {
        $request = $this->client->request($this->method, sprintf("/json1.1/%s", $method), $this->requestData($args));
        $response = $request->getBody()->getContents();
        $this->statusCode = $request->getStatusCode();
        $this->response = $response;
        return $this;

    }

    public function getResponse()
    {
        return $this->response;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    private function requestData($args)
    {
        switch($this->method)
        {
            case 'GET':
                $data = ["http_errors"=> false, 'query' => $args[0]];
                break;
            default:
                $data = ["http_errors"=> false, 'form_params' => $args[0]];
                break;
        }

        $data['query'] = ['apiKey' => $this->apiKey, 'authenticateServiceKey' => $this->serviceKey];
        return $data;
    }

    public function setup($method, $apiKey = null, $serviceKey = null)
    {
        $this->apiKey = null;
        $this->serviceKey = null;
        switch($method)
        {
            default:
                $this->method = 'POST';
                break;
            case 'GET':
                $this->method = 'GET';
                break;
        }
        $this->apiKey = $apiKey;
        $this->serviceKey = $serviceKey;
        return $this;
    }
}