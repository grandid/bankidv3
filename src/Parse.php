<?php

namespace GrandID\Client;

use Exception;

class Parse
{
    private $allowedParams = [
        'callbackUrl' => ['string'],
        'personalNumber' => ['string', 'digits:12'],
        'userVisibleData' => ['string', 'base64'],
        'userNonVisibleData' => ['string', 'base64'],
        'mobileBankId' => ['boolean'],
        'thisDevice' => ['boolean'],
        'deviceChoice' => ['boolean'],
        'gui' => ['boolean'],
        'qr' => ['boolean'],
        'askForSSN' => ['boolean'],
        'customerURL' => ['string', 'base64'],
        'appRedirect' => ['string']
    ];

    // validate function verify if a given parameter is listed in the allowed Parameter in "allowedParms" variable
    // If so it runs parse function in order to verify that the given value of the parameter 
    // respecting its allowed types if it does it will return the same value as a result if not it will return false
    //If the parameter is not listed in the allowed parameters it will throw an exception!

    static function validate($value, $type)
    {
        $function = new static;
        if (isset($function->allowedParams[$type])) {
            return $function->parse($value, $function->allowedParams[$type]);
        } else {
            throw new Exception("Parameter $type does not exist", 100);
        }
    }

    // parse function will verify if a given value is respecting the types of a given parameter or not
    // in case it does it will return the same value if not it will return a false

    private function parse($value, $rules)
    {
        $error = FALSE;
        $result = ['error' => FALSE];
        foreach ($rules as $rule) {
            if (count(explode(":", $rule)) > 1) {
                $rule = explode(":", $rule);
                $allowed = $rule[1];
                $rule = $rule[0];
            } else {
                $rule = $rule;
            }
            switch ($rule) {
                case 'string':
                    if (is_string($value)) {
                        $result['value'][] = (string) $value;
                    } else {
                        $result['error'] = TRUE;
                    }
                    break;
                case 'integer':
                    if (is_numeric($value) || is_int($value)) {
                        $result['value'][] = (int) $value;
                    } else {
                        $result['error'] = TRUE;
                    }
                    break;
                case 'digits':
                    if (strlen($value) == $allowed) {
                        $result['value'][] = $value;
                    } else {
                        $result['error'] = TRUE;
                    }
                    break;
                case 'base64':
                    if (base64_decode($value)) {
                        $result['value'][] = $value;
                    } else {
                        $result['error'] = TRUE;
                    }
                    break;
                case 'boolean':
                    if (is_bool($value)) {
                        $result['value'][] = ($value) ? "true" : "false";
                    } else {
                        $result['error'] = TRUE;
                    }
                    break;
            }
        }
        if ($result['error'] === FALSE) {
            return $result['value'][0];
        }
        return FALSE;
    }
}
