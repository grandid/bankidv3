<?php

namespace GrandID\Client;

use Exception;
use GrandID\Client\Request;

class Response {

    private $request;
    private $username;
    private $session;
    private $userAttributes;
    private $callType;
    private $redirect;
    private $sessionStatus;

    public function setRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    public function generateResponse()
    {
        $response = $this->getRequest()->getResponse();
        $params = json_decode($response,1);
        if(isset($params['errorObject']) && isset($params['errorObject']['message']['status']) && $params['errorObject']['message']['status'] != 'pending') {
            throw new \Exception(json_encode($params));
        }
        switch($this->getCallType())
        {
            case 'FederatedLogin':
                $this->checkErrorObject($params);
                $redirect = (isset($params['redirectUrl'])) ? $params['redirectUrl'] : $params['autoStartToken'];
                $this->setRedirect($redirect);
                $this->setSessionId($params['sessionId']);
            break;
            case 'GetSession':
                if(isset($params['errorObject']['message']) && isset($params['errorObject']['message']['status'])) {
                    $this->setSessionStatus($params['errorObject']['message'], $params['errorObject']['code']);
                } else {
                    $this->setUsername($params['username']);
                    $this->setAttributes($params['userAttributes']);
                    $this->setSessionStatus(['status'=>'success'], 'AUTHENTICATED');
                }
            break;
            case 'Logout':
                
            break;
            default:
                throw new \Exception("Request not allowed, response could not be generated");
            break;
        }
        return $this;
    }

    public function checkErrorObject($response)
    {
        if(!isset($response['errorObject'])) {
            return;
        }
        if(isset($response['errorObject']['code'])) {
            switch($response['errorObject']['code']) {
                case 'APIKEYNOTVALID01':
                    throw new \Exception("API Key or AuthenticateServiceKey are invalid");
                break;
                default:
                    throw new \Exception($response['errorObject']['message']);
                break;
            }
        }
        return;
        
    }

    public function setCallType($type = 'FederatedLogin')
    {
        $this->callType = $type;
        return $this;
    }

    private function getCallType()
    {
        return $this->callType;
    }

    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    public function getRedirect()
    {
        return $this->redirect;
    }

    private function getRequest()
    {
        return $this->request;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        if($this->getCallType() != 'GetSession')
        {
            throw new \Exception("Username is only available after GetSession has been called");
        }
        return !empty($this->username) ? $this->username : false;
    }

    public function setSessionId($session)
    {
        $this->session = $session;
    }

    public function getSessionId()
    {
        return $this->session;
    }

    public function setAttributes($attributes)
    {
        if($this->getCallType() != 'GetSession')
        {
            throw new \Exception("Attributes can only be set after GetSession has been called");
        }
        $this->userAttributes = $attributes;
    }

    public function getUserAttributes()
    {
        if($this->getCallType() != 'GetSession') {
            throw new \Exception("Attributes are available only when using GetSession");
        }
        return $this->userAttributes;
    }

    public function setSessionStatus($status = [], $code = 'BANKID_MSG')
    {
        $this->sessionStatus = ['message'=>$status, 'code'=>$code];
    }

    public function getSessionStatus()
    {
        return $this->sessionStatus;
    }

}