<?php

/**
 *  BankID v3 client is written by Svensk E-Identitet Developers
 *  It is to be used only and specifically programmed for our API.
 *  There are three main functions
 *  @method FederatedLogin      ( apiKey, authenticateServiceKey )
 *  @method GetSession          ( apiKey, authenticateServiceKey, sessionId )
 *  @method Logout              ( apiKey, authenticateServiceKey, sessionId )
 * 
 *  @var string $sessionId      ( sessionId after a FederatedLogin )
 *  @var string $redirectUrl    ( redirectUrl after a FederatedLogin )
 *  @var object $userAttributes ( userAttributes after a GetSession )
 *  @var string $username       ( SSN after a GetSession )
 *  
 *  @param gui
 *  @param callbackUrl
 *  @param personalNumber
 *  @param mobileBankid
 *  @param deviceChoice
 *  @param thisDevice
 *  @param askForSSN
 *  @param userVisibleData
 *  @param userNonVisibleData
 *  @param customerURL
 *  @param qr
 *  @param appRedirect
 * 
 *  @author Svensk E-Identitet
 */

namespace GrandID\Client;

use GuzzleHttp\Client;
use GrandID\Client\Request;
use GrandID\Client\Parse;
use GrandID\Client\Response;
use Exception;

class BankID {

    var $request;
    public $sessionId, $username, $userAttributes, $redirectUrl;
    public $options = [];

    public function __construct($type = "production") 
    {
        $this->request = new Request($type);
    }

    public function __get($name)
    {
        return $this->options[$name];
    }

    public function __isset($name)
    {
        if(isset($this->options[$name])) {
            return true;
        }
        return false;
    }

    public function __set($name, $value)
    {
        $check = Parse::validate($value, $name);
        if($check === FALSE) {
            throw new Exception("Make sure parameter $name has the right type");
        }
        $this->options[$name] = $check;
    }

    /**
     * FederatedLogin
     *
     * @param mixed $apiKey
     * @param mixed $serviceKey
     * @return string redirectUrl or autoStartToken
     */
    public function FederatedLogin($apiKey, $serviceKey)
    {
        if(empty($apiKey) || is_null($apiKey)) {
            throw new Exception("API Key argument is empty or null");
        }
        if(empty($serviceKey) || is_null($serviceKey)) {
            throw new Exception("ServiceKey argument is empty or null");
        }
        try {
            $request = $this->request->setup("POST", $apiKey, $serviceKey)->FederatedLogin($this->options);
        } catch (Exception $e) {
            throw $e;
        }
        return (new Response())->setRequest($request)->setCallType(__FUNCTION__)->generateResponse();
    }

    /**
     * GetSession
     *
     * @param mixed $apiKey
     * @param mixed $serviceKey
     * @param mixed $sessionId
     * @return object userAttributes
     */
    public function GetSession($apiKey, $serviceKey, $sessionId)
    {
        if(empty($sessionId) || is_null($sessionId)) {
            throw new Exception("Session ID Parameter is empty or null");
        }
        if(empty($apiKey) || is_null($apiKey)) {
            throw new Exception("API Key argument is empty or null");
        }
        if(empty($serviceKey) || is_null($serviceKey)) {
            throw new Exception("ServiceKey argument is empty or null");
        }
        try {
            $request = $this->request->setup("POST", $apiKey, $serviceKey)->GetSession(["sessionId"=>$sessionId]);
        } catch (Exception $e) {
            throw $e;
        }
        return (new Response())->setRequest($request)->setCallType(__FUNCTION__)->generateResponse();
    }

    /**
     * Logout
     *
     * @param mixed $apiKey
     * @param mixed $serviceKey
     * @param mixed $sessionId
     * @return bool true/false
     */
    public function Logout($apiKey, $serviceKey, $sessionId)
    {
        if(empty($sessionId) || is_null($sessionId)) {
            throw new Exception("Session ID Parameter is empty or null");
        }
        try {
            $request = $this->request->setup("POST", $apiKey, $serviceKey)->Logout(["sessionId"=>$sessionId]);
        } catch (Exception $e) {
            throw $e;
        }
        return true;
    }

    /* private function parseSession($request)
    {
        $response = $request->getResponse();
        $object = json_decode($response);
        if($request->statusCode !== 200) {
            throw new Exception($response);
        }
        if(!is_array($object) && !is_object($object)) {
            throw new Exception("Response is not a json object, Response: ");
        }
        if(isset($object->sessionId) && !empty($object->sessionId)) {
            $this->sessionId = $object->sessionId;
            if(isset($object->redirectUrl)) {
                $this->redirectUrl = $object->redirectUrl;
                return $object->redirectUrl;
            } elseif (isset($object->autoStartToken)) {
                return $object->autoStartToken;
            } elseif (isset($object->username) && isset($object->userAttributes)) {
                $this->username = $object->username;
                $this->userAttributes = $object->userAttributes;
                return $object->userAttributes;
            }
        }
        if(isset($object->errorObject)) {
            return json_encode($object->errorObject);
        }
        if(isset($object->errorObject->code) && isset($object->errorObject->message)) {
            throw new Exception($object->message);
        }
    } */

}
