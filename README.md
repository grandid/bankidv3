# BankID v3
Add this to your composer
```json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:grandid/bankidv3.git"		
        }
    ],
    "require": {
        "grandid/bankidv3": "dev-master",
        "guzzlehttp/guzzle": "*"
    }
}
```
and run 
>composer install

To use it in your application you should instanciate the bankid class:

```php
 /**
 *  BankID v3 client is written by Svensk E-Identitet Developers
 *  It is to be used only and specifically programmed for our API.
 *  There are three main functions
 *  @method FederatedLogin      ( apiKey, authenticateServiceKey )
 *  @method GetSession          ( apiKey, authenticateServiceKey, sessionId )
 *  @method Logout              ( apiKey, authenticateServiceKey, sessionId )
 * 
 *  @param gui
 *  @param callbackUrl
 *  @param personalNumber
 *  @param mobileBankid
 *  @param deviceChoice
 *  @param thisDevice
 *  @param askForSSN
 *  @param userVisibleData
 *  @param userNonVisibleData
 *  @param customerURL
 */
 
require 'vendor/autoload.php';

use GrandID\Client\BankID;

$bankid = new BankID($env = "production");
// set parameters
$bankid->gui = false;
$bankid->personalNumber = "199001011234";

/**
 * @method getRedirect()
 * @method getSessionId()
 * @return \GrandID\Client\Response
 * 
 */
$response = $bankid->FederatedLogin("API KEY", "SERVICE KEY"); // string
```
~~~~
>/**
> $sessionId = $response->getSessionId(); // returns the session id
> $redirectUrl = $response->getRedirect; // returns the redirect Url or autoStartToken if bankid for apps is used ( $bankid->gui = false )
>*/
~~~~
 ***
 
```php
/**
 *  Is called after Federated Login
 * @param string $apiKey
 * @param string @serviceKey
 * @param string @sessionId
 * @method getUsername()
 * @method getUserAttributes()
 * @method getSessionId()
 * @return \GrandID\Client\Response
 */
$response = $bankid->GetSession("API KEY", "SERVICE KEY", $bankid->getSessionId()); 
```
~~~~
>/**
> $attributes = $response->getUserAttributes();
> $sessionId = $response->getSessionId(); // returns the sessionid
> $username = $response->getUsername(); // username
>*/
~~~~
