<?php

use PHPUnit\Framework\TestCase;
use GrandID\Client\Parse;
use GrandID\Client\BankID;
use Jchook\AssertThrows\AssertThrows;

// To run this test use this command: 
//./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Phpunit/ParseTest.php

class ParseTest extends TestCase
{
    // In this test we will check 3 cases
    // 1st case: A parameter that is listed in the allowed parameters with a value that respecting its types
    // 2nd case: A parameter that is listed in the allowed parameters with a value that doesn't respect its types
    // 3rd case: A parameter that is not listed in the allowed parameters

    public function testValidate()
    {

        //
        $bankid = new BankID($env = "production");
        $bankid->personalNumber = 197512064671;
        $this->expectExceptionMessage("Make sure parameter personalNumber has the right type");
        
        // 1st case: A parameter that is listed in the allowed parameters with a value that respecting its types
        $param = 'https://admin2.local.grandid.com/session/explorer/callback';
        $parse = new Parse();
        $this->assertEquals($param, $parse->validate($param, 'callbackUrl'));

        // 2nd case: A parameter that is listed in the allowed parameters with a value that doesn't respect its types
        $param = 123456789;
        $this->assertEquals(false, $parse->validate($param, 'callbackUrl'));

        // 3rd case: A parameter that is not listed in the allowed parameters
        $this->expectException("Exception");
        $this->expectExceptionCode(100);
        $this->expectExceptionMessage("Parameter callbackUrllll does not exist");

        // the expectations have been set up, now run the code
        // that should throw the exception
        $this->assertEquals(false, $parse->validate($param, 'callbackUrllll'));
        
    }
}
